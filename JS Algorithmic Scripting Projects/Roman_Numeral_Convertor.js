/*

Author: https://github.com/bbbeesh

Source: https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects/roman-numeral-converter

Problem Statement:

Convert the given number into a roman numeral.

All roman numerals (RN) answers should be provided in upper-case.

*/

function convertToRoman(num) {

  //These two lists contain all the possible combinations of unique representations and their corresponding integer values.
  let R = ['M','CM','D','CD','C','XC','L','XL','X','IX','V','IV','I'];
  let I = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];

  //The answer variable is initialized to an empty string
  let Ans = '';
  
  //We loop throuh the I values and add the corresponding RN to the answer string
  for(let i = 0; i<I.length; i++) {
    
    //Find the number of occurances of the RN
    let x = Math.floor(num/I[i]);
    
    //Add as many copies of the RN to the Answer
    Ans += R[i].repeat(x); 
    
    //Update the number
    num = num % I[i]; 
  }

  return Ans;
}

//Test Cases
convertToRoman(2); // should return the string II.
convertToRoman(3); // should return the string III.
convertToRoman(4); // should return the string IV.
convertToRoman(5); // should return the string V.
convertToRoman(9); // should return the string IX.
convertToRoman(12); // should return the string XII.
convertToRoman(16); // should return the string XVI.
convertToRoman(29); // should return the string XXIX.
convertToRoman(44); // should return the string XLIV.
convertToRoman(45); // should return the string XLV.
convertToRoman(68); // should return the string LXVIII
convertToRoman(83); // should return the string LXXXIII
convertToRoman(97); // should return the string XCVII
convertToRoman(99); // should return the string XCIX
convertToRoman(400); // should return the string CD
convertToRoman(500); // should return the string D
convertToRoman(501); // should return the string DI
convertToRoman(649); // should return the string DCXLIX
convertToRoman(798); // should return the string DCCXCVIII
convertToRoman(891); // should return the string DCCCXCI
convertToRoman(1000); // should return the string M
convertToRoman(1004); // should return the string MIV
convertToRoman(1006); // should return the string MVI
convertToRoman(1023); // should return the string MXXIII
convertToRoman(2014); // should return the string MMXIV
convertToRoman(3999); // should return the string MMMCMXCIX
