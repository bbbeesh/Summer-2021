/*

Author: https://github.com/bbbeesh

Source: https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects/palindrome-checker

Problem Statement:

Return true if the given string is a palindrome. Otherwise, return false.

A palindrome is a word or sentence that's spelled the same way both forward and backward, ignoring punctuation, case, and spacing.

Note: You'll need to remove all non-alphanumeric characters (punctuation, spaces and symbols) and turn everything into the same case (lower or upper case) in order to check for palindromes.

We'll pass strings with varying formats, such as racecar, RaceCar, and race CAR among others.

We'll also pass strings with special symbols, such as 2A3*3a2, 2A3 3a2, and 2_A3*3#A2.

*/

function palindrome(str) {
  
  //Removing all non-alphanumeric characters using regular expressions
  str = str.replace(/[^0-9a-z]/gi, '');
  
  /* RegEx Explanation:
  
  ^   - Negation; Matches all characters not included in the set
  0-9 - All numbers in the range '0' to '9'.
  a-z - All characters in the range 'a' to 'z'. Case sensitive by default.
  g   - Global flag; the search looks for all matches, without it only the first match is returned. 
  i   - Ignore-casing flag; makes the search case insensitive.
  
  We search the entire string for all the characters that are not numbers or alphabets 
  regardless of its case, and we remove them by replacing with a null '' character. */
  
  //Converting the string to lowercase to avoid case sensitivity
  str = str.toLowerCase();

  //The string is split into a list, reversed and combined into a new string
  let temp = str.split("");
  temp = temp.reverse();
  temp = temp.join("");
  
  //The if the reversed string is the same as the original, it is a palindrome
  return temp == str;
}

//Tests
palindrome("eye"); // should return a boolean.
palindrome("eye"); // should return true.
palindrome("_eye"); // should return true.
palindrome("race car"); // should return true.
palindrome("not a palindrome"); // should return false.
palindrome("A man, a plan, a canal. Panama"); // should return true.
palindrome("never odd or even"); // should return true.
palindrome("nope"); // should return false.
palindrome("almostomla"); // should return false.
palindrome("My age is 0, 0 si ega ym."; // should return true.
palindrome("1 eye for of 1 eye."); // should return false.
palindrome("0_0 (: /-\ :) 0-0"); // should return true.
palindrome("five|\_/|four"); // should return false.
